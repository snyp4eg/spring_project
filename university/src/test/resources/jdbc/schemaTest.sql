DROP TABLE course IF EXISTS;
DROP TABLE employee IF EXISTS;
DROP TABLE position IF EXISTS;
DROP TABLE employee_position IF EXISTS;
DROP TABLE groups IF EXISTS;
DROP TABLE room_type IF EXISTS;
DROP TABLE room IF EXISTS;
DROP TABLE teacher IF EXISTS;

CREATE TABLE employee
(
    id int NOT NULL PRIMARY KEY,
    firstName varchar(255),
    lastName varchar(255)
);
CREATE TABLE position
(
    id int NOT NULL PRIMARY KEY,
    name varchar(255)
);
CREATE TABLE employee_position (
    employee_id INT REFERENCES employee,
    position_id INT REFERENCES position,
    PRIMARY KEY(employee_id, position_id)
);
CREATE TABLE groups
(
    id int NOT NULL PRIMARY KEY,
    name varchar(255)
);
CREATE TABLE room_type
(
    id int NOT NULL PRIMARY KEY,
    name varchar(255)
);
CREATE TABLE room
(
    id int NOT NULL PRIMARY KEY,
    capacity int NOT NULL,
    room_type_id int REFERENCES room_type
);
CREATE TABLE teacher
(
    id int NOT NULL PRIMARY KEY,
    employee_id int REFERENCES employee
);
CREATE TABLE course
(
    id int NOT NULL PRIMARY KEY,
    name varchar(255),
    capacity int,
    teacher_id int REFERENCES teacher
);
CREATE TABLE student
(
    id int NOT NULL PRIMARY KEY,
    group_id int REFERENCES groups,
    course_id int REFERENCES course,
    first_name varchar(255),
    last_name varchar(255)
);