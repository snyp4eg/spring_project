package com.gmail.snyp4eg.university.dao;

import static org.junit.jupiter.api.Assertions.assertEquals;

import java.util.ArrayList;
import java.util.List;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import com.gmail.snyp4eg.university.config.DaoTestConfig;
import com.gmail.snyp4eg.university.model.Group;
import com.gmail.snyp4eg.university.reader.PropertyReader;

@ExtendWith(SpringExtension.class)
@ContextConfiguration(classes = DaoTestConfig.class)
@DirtiesContext(classMode = DirtiesContext.ClassMode.AFTER_EACH_TEST_METHOD)
@SpringBootTest
public class GroupDaoTest {
  private GroupDao groupDao;
  @Autowired
  private JdbcTemplate jdbcTemplateTest;
  @Autowired
  private PropertyReader queryReaderTest;
  
  @BeforeEach
  public void init() {
    groupDao = new GroupDao(jdbcTemplateTest, queryReaderTest);
  }
    
  @Test
  void shouldReturnExpectedGroupThenGetByIdCalled() {
    Group expectedGroup = new Group(1, "10-AS");
    assertEquals(groupDao.getById(1), expectedGroup);
  }

  @Test
  void shouldReturnExpectedGroupListThenGetAllCalled() {
    Group groupOne = new Group(1, "10-AS");
    Group groupTwo = new Group(2, "11-DF");
    Group groupThree = new Group(3, "12-FR");
    List<Group> expectedGroupList = new ArrayList<>();
    expectedGroupList.add(groupOne);
    expectedGroupList.add(groupTwo);
    expectedGroupList.add(groupThree);
    assertEquals(groupDao.getAll(), expectedGroupList);
  }

  @Test
  void shouldCreateNewDataThenInsertNewGroupCalled() {
    Group addGroup = new Group(4, "Nothing");
    groupDao.insert(addGroup);
    assertEquals(groupDao.getById(4), addGroup);
  }

  @Test
  void shouldUpdateExistDataThenUpdateGroupCalled() {
    Group updateGroup = new Group(1, "Math");
    groupDao.update(updateGroup);
    assertEquals(groupDao.getById(1), updateGroup);
  }

  @Test
  void shouldDeleteExistDataThenDeleteGroupCalled() {
    Group deleteGroup = new Group(2, "11-DF");
    groupDao.delete(deleteGroup);
    assertEquals(groupDao.getById(2), null);
  }
}
