package com.gmail.snyp4eg.university.dao;

import static org.junit.Assert.assertEquals;
import java.util.ArrayList;
import java.util.List;

import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.jdbc.core.JdbcTemplate;

import com.gmail.snyp4eg.university.config.DaoTestConfig;
import com.gmail.snyp4eg.university.model.Course;
import com.gmail.snyp4eg.university.reader.PropertyReader;

@ExtendWith(SpringExtension.class)
@ContextConfiguration(classes = DaoTestConfig.class)
@DirtiesContext(classMode = DirtiesContext.ClassMode.AFTER_EACH_TEST_METHOD)
@SpringBootTest
public class CourseDaoTest {
  private CourseDao testCourseDao;
  @Autowired
  private JdbcTemplate jdbcTemplateTest;
  @Autowired
  private PropertyReader queryReaderTest;
  
  @BeforeEach
  public void init() {
  testCourseDao = new CourseDao(jdbcTemplateTest, queryReaderTest);
  }
  
  @Test
  void shouldReturnExpectedCourseThenGetByIdCalled() {
    Course expectedCourse = new Course(1, "Math", 20, 1);
    assertEquals(testCourseDao.getById(1), expectedCourse);
  }

  @Test
  void shouldReturnExpectedCourseListThenGetAllCalled() {
    Course courseOne = new Course(1, "Math", 20, 1);
    Course courseTwo = new Course(2, "Art", 30, 2);
    Course courseThree = new Course(3, "Algebra", 40, 1);
    Course courseFour = new Course(4, "Geometry", 50, 3);
    List<Course> expectedCoursesList = new ArrayList<>();
    expectedCoursesList.add(courseOne);
    expectedCoursesList.add(courseTwo);
    expectedCoursesList.add(courseThree);
    expectedCoursesList.add(courseFour);
    assertEquals(testCourseDao.getAll(), expectedCoursesList);
  }

  @Test
  void shouldCreateNewDataThenInsertNewCourseCalled() {
    Course addCourse = new Course(5, "Nothing", 100, 2);
    testCourseDao.insert(addCourse);
    assertEquals(testCourseDao.getById(5), addCourse);
  }

  @Test
  void shouldUpdateExistDataThenUpdateCourseCalled() {
    Course updateCourse = new Course(1, "Math", 200, 1);
    testCourseDao.update(updateCourse);
    assertEquals(testCourseDao.getById(1), updateCourse);
  }

  @Test
  void shouldDeleteExistDataThenDeleteCourseCalled() {
    Course deleteCourse = new Course(2, "Art", 30, 2);
    testCourseDao.delete(deleteCourse);
    assertEquals(testCourseDao.getById(2), null);
  }
}
