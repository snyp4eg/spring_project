package com.gmail.snyp4eg.university.dao;

import static org.junit.jupiter.api.Assertions.assertEquals;

import java.util.ArrayList;
import java.util.List;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import com.gmail.snyp4eg.university.config.DaoTestConfig;
import com.gmail.snyp4eg.university.model.Position;
import com.gmail.snyp4eg.university.reader.PropertyReader;

@ExtendWith(SpringExtension.class)
@ContextConfiguration(classes = DaoTestConfig.class)
@DirtiesContext(classMode = DirtiesContext.ClassMode.AFTER_EACH_TEST_METHOD)
@SpringBootTest
public class PositionDaoTest {
  private PositionDao positionDao;
  @Autowired
  private JdbcTemplate jdbcTemplateTest;
  @Autowired
  private PropertyReader queryReaderTest;
  
  @BeforeEach
  public void init() {
    positionDao = new PositionDao(jdbcTemplateTest, queryReaderTest);
  }
  
  @Test
  void shouldReturnExpectedPositionThenGetByIdCalled() {
    Position expectedPosition = new Position(1, "teacher");
    assertEquals(positionDao.getById(1), expectedPosition);
  }

  @Test
  void shouldReturnExpectedPositionListThenGetAllCalled() {
    Position positionOne = new Position(1, "teacher");
    Position positionTwo = new Position(2, "laboraunt");
    Position positionThree = new Position(3, "staff");
    List<Position> expectedPositionList = new ArrayList<>();
    expectedPositionList.add(positionOne);
    expectedPositionList.add(positionTwo);
    expectedPositionList.add(positionThree);
    assertEquals(positionDao.getAll(), expectedPositionList);
  }

  @Test
  void shouldCreateNewDataThenInsertNewPositionCalled() {
    Position addPosition = new Position(4, "Nothing");
    positionDao.insert(addPosition);
    assertEquals(positionDao.getById(4), addPosition);
  }

  @Test
  void shouldUpdateExistDataThenUpdatePositionCalled() {
    Position updatePosition = new Position(1, "Math");
    positionDao.update(updatePosition);
    assertEquals(positionDao.getById(1), updatePosition);
  }

  @Test
  void shouldDeleteExistDataThenDeletePositionCalled() {
    Position deletePosition = new Position(2, "laboraunt");
    positionDao.delete(deletePosition);
    assertEquals(positionDao.getById(2), null);
  }

  @Test
  void shouldReturnExpectedPositionListThenGetPositionsListForEmployeeCalled() {
    Position positionOne = new Position(1, "teacher");
    Position positionTwo = new Position(2, "laboraunt");
    List<Position> expectedPositionList = new ArrayList<>();
    expectedPositionList.add(positionOne);
    expectedPositionList.add(positionTwo);
    assertEquals(positionDao.getPositionsListForEmployee(2), expectedPositionList);
  }
}
