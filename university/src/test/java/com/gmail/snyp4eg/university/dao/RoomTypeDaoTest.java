package com.gmail.snyp4eg.university.dao;

import static org.junit.jupiter.api.Assertions.assertEquals;
import java.util.ArrayList;
import java.util.List;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import com.gmail.snyp4eg.university.config.DaoTestConfig;
import com.gmail.snyp4eg.university.model.RoomType;
import com.gmail.snyp4eg.university.reader.PropertyReader;

@ExtendWith(SpringExtension.class)
@ContextConfiguration(classes = DaoTestConfig.class)
@DirtiesContext(classMode = DirtiesContext.ClassMode.AFTER_EACH_TEST_METHOD)
@SpringBootTest
public class RoomTypeDaoTest {
  private RoomTypeDao roomTypeDao;
  @Autowired
  private JdbcTemplate jdbcTemplateTest;
  @Autowired
  private PropertyReader queryReaderTest;
  
  @BeforeEach
  public void init() {
    roomTypeDao = new RoomTypeDao(jdbcTemplateTest, queryReaderTest);
  }
    
  @Test
  void shouldReturnExpectedRoomTypeThenGetByIdCalled() {
    RoomType expectedPosition = new RoomType(1, "lectory");
    assertEquals(roomTypeDao.getById(1), expectedPosition);
  }

  @Test
  void shouldReturnExpectedRoomTypeListThenGetAllCalled() {
    RoomType roomTypeOne = new RoomType(1, "lectory");
    RoomType roomTypeTwo = new RoomType(2, "laboratory");
    RoomType roomTypeThree = new RoomType(3, "staff");
    List<RoomType> expectedRoomTypeList = new ArrayList<>();
    expectedRoomTypeList.add(roomTypeOne);
    expectedRoomTypeList.add(roomTypeTwo);
    expectedRoomTypeList.add(roomTypeThree);
    assertEquals(roomTypeDao.getAll(), expectedRoomTypeList);
  }

  @Test
  void shouldCreateNewDataThenInsertNewRoomTypeCalled() {
    RoomType addRoomType = new RoomType(4, "Nothing");
    roomTypeDao.insert(addRoomType);
    assertEquals(roomTypeDao.getById(4), addRoomType);
  }

  @Test
  void shouldUpdateExistDataThenUpdateRoomTypeCalled() {
    RoomType updateRoomType = new RoomType(1, "Math");
    roomTypeDao.update(updateRoomType);
    assertEquals(roomTypeDao.getById(1), updateRoomType);
  }

  @Test
  void shouldDeleteExistDataThenDeleteRoomTypeCalled() {
    RoomType deleteRoomType = new RoomType(2, "laboratory");
    roomTypeDao.delete(deleteRoomType);
    assertEquals(roomTypeDao.getById(2), null);
  }
}
