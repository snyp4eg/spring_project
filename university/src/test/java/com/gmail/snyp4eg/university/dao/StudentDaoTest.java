package com.gmail.snyp4eg.university.dao;

import static org.junit.jupiter.api.Assertions.assertEquals;

import java.util.ArrayList;
import java.util.List;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import com.gmail.snyp4eg.university.config.DaoTestConfig;
import com.gmail.snyp4eg.university.model.Student;
import com.gmail.snyp4eg.university.reader.PropertyReader;

@ExtendWith(SpringExtension.class)
@ContextConfiguration(classes = DaoTestConfig.class)
@DirtiesContext(classMode = DirtiesContext.ClassMode.AFTER_EACH_TEST_METHOD)
@SpringBootTest
public class StudentDaoTest {
  private StudentDao studentDao;
  @Autowired
  private JdbcTemplate jdbcTemplateTest;
  @Autowired
  private PropertyReader queryReaderTest;
  
  @BeforeEach
  public void init() {
    studentDao = new StudentDao(jdbcTemplateTest, queryReaderTest);
  }
  
  @Test
  void shouldReturnExpectedStudentTypeThenGetByIdCalled() {
    Student expectedStudent = new Student(1, 1, 1, "Andrey", "Tukin");
    assertEquals(studentDao.getById(1), expectedStudent);
  }

  @Test
  void shouldReturnExpectedStudentTypeListThenGetAllCalled() {
    Student studentOne = new Student(1, 1, 1, "Andrey", "Tukin");
    Student studentTwo = new Student(2, 1, 1, "Bone", "Pukin");
    Student studentThree = new Student(3, 2, 2, "Jone", "Zukin");
    Student studentFour = new Student(4, 3, 3, "Jane", "Trukin");
    List<Student> expectedStudentList = new ArrayList<>();
    expectedStudentList.add(studentOne);
    expectedStudentList.add(studentTwo);
    expectedStudentList.add(studentThree);
    expectedStudentList.add(studentFour);
    assertEquals(studentDao.getAll(), expectedStudentList);
  }

  @Test
  void shouldCreateNewDataThenInsertNewStudentCalled() {
    Student addStudent = new Student(5, 2, 3, "Jone", "Prukin");
    studentDao.insert(addStudent);
    assertEquals(studentDao.getById(5), addStudent);
  }

  @Test
  void shouldUpdateExistDataThenUpdateStudentCalled() {
    Student updateStudent = new Student(1, 2, 1, "Andrey", "Tukin");
    studentDao.update(updateStudent);
    assertEquals(studentDao.getById(1), updateStudent);
  }

  @Test
  void shouldDeleteExistDataThenDeleteStudentCalled() {
    Student deleteStudent = new Student(3, 2, 2, "Jone", "Zukin");
    studentDao.delete(deleteStudent);
    assertEquals(studentDao.getById(3), null);
  }

  void shouldReturnExpectedStudentListThenGetCouurseStudentListCalled() {
    Student studentOne = new Student(1, 1, "Andrey", "Tukin");
    Student studentTwo = new Student(2, 1, "Bone", "Pukin");
    List<Student> expectedStudentList = new ArrayList<>();
    expectedStudentList.add(studentOne);
    expectedStudentList.add(studentTwo);
    assertEquals(studentDao.getCourseStudentList(1), expectedStudentList);
  }
}
