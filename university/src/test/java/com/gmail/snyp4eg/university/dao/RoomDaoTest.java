package com.gmail.snyp4eg.university.dao;

import static org.junit.jupiter.api.Assertions.assertEquals;
import java.util.ArrayList;
import java.util.List;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import com.gmail.snyp4eg.university.config.DaoTestConfig;
import com.gmail.snyp4eg.university.model.Room;
import com.gmail.snyp4eg.university.reader.PropertyReader;

@ExtendWith(SpringExtension.class)
@ContextConfiguration(classes = DaoTestConfig.class)
@DirtiesContext(classMode = DirtiesContext.ClassMode.AFTER_EACH_TEST_METHOD)
@SpringBootTest
public class RoomDaoTest {
  private RoomDao roomDao;
  @Autowired
  private JdbcTemplate jdbcTemplateTest;
  @Autowired
  private PropertyReader queryReaderTest;
  
  @BeforeEach
  public void init() {
    roomDao = new RoomDao(jdbcTemplateTest, queryReaderTest);
  }
  
  @Test
  void shouldReturnExpectedRoomThenGetByIdCalled() {
    Room expectedRoom = new Room(1, 50, 1);
    assertEquals(roomDao.getById(1), expectedRoom);
  }

  @Test
  void shouldReturnExpectedRoomListThenGetAllCalled() {
    Room roomOne = new Room(1, 50, 1);
    Room roomTwo = new Room(2, 20, 2);
    Room roomThree = new Room(3, 0, 3);
    List<Room> expectedRoomList = new ArrayList<>();
    expectedRoomList.add(roomOne);
    expectedRoomList.add(roomTwo);
    expectedRoomList.add(roomThree);
    assertEquals(roomDao.getAll(), expectedRoomList);
  }

  @Test
  void shouldCreateNewDataThenInsertNewRoomCalled() {
    Room addRoom = new Room(4, 100, 1);
    roomDao.insert(addRoom);
    assertEquals(roomDao.getById(4), addRoom);
  }

  @Test
  void shouldUpdateExistDataThenUpdateRoomCalled() {
    Room updateRoom = new Room(1, 110, 1);
    roomDao.update(updateRoom);
    assertEquals(roomDao.getById(1), updateRoom);
  }

  @Test
  void shouldDeleteExistDataThenDeleteRoomCalled() {
    Room deleteRoom = new Room(2, 20, 2);
    roomDao.delete(deleteRoom);
    assertEquals(roomDao.getById(2), null);
  }
}
