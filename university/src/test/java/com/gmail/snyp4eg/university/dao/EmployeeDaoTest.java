package com.gmail.snyp4eg.university.dao;

import static org.junit.jupiter.api.Assertions.assertEquals;

import java.util.ArrayList;
import java.util.List;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import com.gmail.snyp4eg.university.config.DaoTestConfig;
import com.gmail.snyp4eg.university.model.Employee;
import com.gmail.snyp4eg.university.reader.PropertyReader;

@ExtendWith(SpringExtension.class)
@ContextConfiguration(classes = DaoTestConfig.class)
@DirtiesContext(classMode = DirtiesContext.ClassMode.AFTER_EACH_TEST_METHOD)
@SpringBootTest
public class EmployeeDaoTest {
  private EmployeeDao testEmployeeDao;
  @Autowired
  private JdbcTemplate jdbcTemplateTest;
  @Autowired
  private PropertyReader queryReaderTest;
  
  @BeforeEach
  public void init() {
    testEmployeeDao = new EmployeeDao(jdbcTemplateTest, queryReaderTest);
  }
  
  @Test
  void shouldReturnExpectedEmployeeThenGetByIdCalled() {
  Employee expectedEmployee = new Employee(1, "John", "Snow");
  assertEquals(testEmployeeDao.getById(1), expectedEmployee);
  }

  @Test
  void shouldReturnExpectedEmployeeListThenGetAllCalled() {
    Employee employeeOne = new Employee(1, "John", "Snow");
    Employee employeeTwo = new Employee(2, "Jane", "Rain");
    Employee employeeThree = new Employee(3, "June", "Winter");
    Employee employeeFour = new Employee(4, "Ugin", "April");
  List<Employee> expectedEmployersList = new ArrayList<>();
  expectedEmployersList.add(employeeOne);
  expectedEmployersList.add(employeeTwo);
  expectedEmployersList.add(employeeThree);
  expectedEmployersList.add(employeeFour);
  assertEquals(testEmployeeDao.getAll(), expectedEmployersList);
  }

  @Test
  void shouldCreateNewDataThenInsertNewEmployeeCalled() {
    Employee addEmployee = new Employee(5, "Jane", "Dow");
    testEmployeeDao.insert(addEmployee);
  assertEquals(testEmployeeDao.getById(5), addEmployee);
  }

  @Test
  void shouldUpdateExistDataThenUpdateEmployeeCalled() {
    Employee updateEmployee = new Employee(1, "John", "Dow");
    testEmployeeDao.update(updateEmployee);
  assertEquals(testEmployeeDao.getById(1), updateEmployee);
  }

  @Test
  void shouldDeleteExistDataThenDeleteEmployeeCalled() {
    Employee deleteEmployee = new Employee(2, "Jane", "Rain");
    testEmployeeDao.delete(deleteEmployee);
  assertEquals(testEmployeeDao.getById(2), null);
  }
  
  @Test
  void shouldReturnExpectedEmployeeListThenGetEmployersOnPositionListCalled() {
    Employee employeeOne = new Employee(1, "John", "Snow");
    Employee employeeTwo = new Employee(2, "Jane", "Rain");
    Employee employeeThree = new Employee(3, "June", "Winter");
    List<Employee> expectedEmployersList = new ArrayList<>();
  expectedEmployersList.add(employeeOne);
  expectedEmployersList.add(employeeTwo);
  expectedEmployersList.add(employeeThree);
  assertEquals(testEmployeeDao.getEmployersOnPositionList(1), expectedEmployersList);
  }
}
