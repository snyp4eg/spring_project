package com.gmail.snyp4eg.university.dao;

import static org.junit.jupiter.api.Assertions.assertEquals;

import java.util.ArrayList;
import java.util.List;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import com.gmail.snyp4eg.university.config.DaoTestConfig;
import com.gmail.snyp4eg.university.model.Teacher;
import com.gmail.snyp4eg.university.reader.PropertyReader;

@ExtendWith(SpringExtension.class)
@ContextConfiguration(classes = DaoTestConfig.class)
@DirtiesContext(classMode = DirtiesContext.ClassMode.AFTER_EACH_TEST_METHOD)
@SpringBootTest
public class TeacherDaoTest {
  private TeacherDao teacherDao;
  @Autowired
  private JdbcTemplate jdbcTemplateTest;
  @Autowired
  private PropertyReader queryReaderTest;
  
  @BeforeEach
  public void init() {
    teacherDao = new TeacherDao(jdbcTemplateTest, queryReaderTest);
  }
    
  @Test
  void shouldReturnExpectedTeacherTypeThenGetByIdCalled() {
    teacherDao = new TeacherDao(jdbcTemplateTest, queryReaderTest);
    Teacher expectedTeacher = new Teacher(1, 1);
    assertEquals(teacherDao.getById(1), expectedTeacher);
  }

  @Test
  void shouldReturnExpectedTeacherTypeListThenGetAllCalled() {
    teacherDao = new TeacherDao(jdbcTemplateTest, queryReaderTest);
    Teacher teacherOne = new Teacher(1, 1);
    Teacher teacherTwo = new Teacher(2, 2);
    Teacher teacherThree = new Teacher(3, 3);
    List<Teacher> expectedTeacherList = new ArrayList<>();
    expectedTeacherList.add(teacherOne);
    expectedTeacherList.add(teacherTwo);
    expectedTeacherList.add(teacherThree);
    assertEquals(teacherDao.getAll(), expectedTeacherList);
  }

  @Test
  void shouldCreateNewDataThenInsertNewTeacherTypeCalled() {
    teacherDao = new TeacherDao(jdbcTemplateTest, queryReaderTest);
    Teacher addTeacher = new Teacher(4, 1);
    teacherDao.insert(addTeacher);
    assertEquals(teacherDao.getById(4), addTeacher);
  }

  @Test
  void shouldUpdateExistDataThenUpdateTeacherTypeCalled() {
    teacherDao = new TeacherDao(jdbcTemplateTest, queryReaderTest);
    Teacher updateTeacher = new Teacher(1, 2);
    teacherDao.update(updateTeacher);
    assertEquals(teacherDao.getById(1), updateTeacher);
  }

  @Test
  void shouldDeleteExistDataThenDeleteTeacherTypeCalled() {
    teacherDao = new TeacherDao(jdbcTemplateTest, queryReaderTest);
    Teacher deleteTeacher = new Teacher(2, 2);
    teacherDao.delete(deleteTeacher);
    assertEquals(teacherDao.getById(2), null);
  }
}
