package com.gmail.snyp4eg.university.dao.interfaces;

import java.util.List;
import com.gmail.snyp4eg.university.model.Position;

public interface PositionExtendedDao extends GenericDao<Position>{
  
  List<Position> getPositionsListForEmployee(Integer employeeId);
}
