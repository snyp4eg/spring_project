package com.gmail.snyp4eg.university.dao;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.jdbc.core.JdbcTemplate;

import com.gmail.snyp4eg.university.dao.interfaces.StudentExtendedDao;
import com.gmail.snyp4eg.university.exception.DaoException;
import com.gmail.snyp4eg.university.model.Student;
import com.gmail.snyp4eg.university.reader.PropertyReader;
import com.gmail.snyp4eg.university.util.StudentMapper;

public class StudentDao implements StudentExtendedDao {
    private static final String GET_BY_ID_KEY = "student.get-by-id";
    private static final String GET_ALL_KEY = "student.get-all";
    private static final String INSERT_KEY = "student.insert";
    private static final String UPDATE_KEY = "student.update";
    private static final String DELETE_KEY = "student.delete";
    private static final String GET_COURSE_STUDENT_LIST_KEY = "student.course-student-list";
    private JdbcTemplate jdbcTemplate;
    private PropertyReader queryReader;
    
    @Autowired
    public StudentDao(JdbcTemplate jdbcTemplate, PropertyReader queryReader) {
      this.jdbcTemplate = jdbcTemplate;
      this.queryReader = queryReader;
    }

    @Override
    public Student getById(Integer id) {
	Student student;
	try {
	    student = jdbcTemplate.queryForObject(queryReader.read(GET_BY_ID_KEY), new StudentMapper(), id);
	} catch (EmptyResultDataAccessException e) {
	  throw new DaoException(e);
	}
	return student;
    }

    @Override
    public List<Student> getAll() {
	return jdbcTemplate.query(queryReader.read(GET_ALL_KEY), new StudentMapper());
    }

    @Override
    public void insert(Student student) {
	jdbcTemplate.update(queryReader.read(INSERT_KEY), student.getId(), student.getGroupId(), student.getCourseId(), student.getFirstName(),
		student.getLastName());
    }

    @Override
    public void update(Student student) {
	jdbcTemplate.update(queryReader.read(UPDATE_KEY), student.getGroupId(), student.getCourseId(), student.getFirstName(),
		student.getLastName(), student.getId());

    }

    @Override
    public void delete(Student student) {
	jdbcTemplate.update(queryReader.read(DELETE_KEY), student.getId());
    }

    @Override
    public List<Student> getCourseStudentList(Integer courseId) {
      return jdbcTemplate.query(queryReader.read(GET_COURSE_STUDENT_LIST_KEY), new StudentMapper(), courseId);
    }
}
