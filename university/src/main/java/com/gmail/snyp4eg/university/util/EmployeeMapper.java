package com.gmail.snyp4eg.university.util;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;
import com.gmail.snyp4eg.university.model.Employee;

public class EmployeeMapper implements RowMapper<Employee> {
    private static final String ID = "id";
    private static final String FIRST_NAME = "firstName";
    private static final String LAST_NAME = "lastName";

    @Override
    public Employee mapRow(ResultSet rs, int rowNum) throws SQLException {
	Integer id = rs.getInt(ID);
	String firstName = rs.getString(FIRST_NAME);
	String lastName = rs.getString(LAST_NAME);
	return new Employee(id, firstName, lastName);
    }
}
