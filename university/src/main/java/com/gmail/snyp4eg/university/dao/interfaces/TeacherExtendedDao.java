package com.gmail.snyp4eg.university.dao.interfaces;

import com.gmail.snyp4eg.university.model.Teacher;

public interface TeacherExtendedDao extends GenericDao<Teacher>{
   
}
