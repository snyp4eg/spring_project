package com.gmail.snyp4eg.university.main;

import java.util.List;

import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import com.gmail.snyp4eg.university.config.DaoConfig;
import com.gmail.snyp4eg.university.dao.CourseDao;
import com.gmail.snyp4eg.university.dao.TeacherDao;
import com.gmail.snyp4eg.university.model.Course;
import com.gmail.snyp4eg.university.model.Teacher;

public class Main {

    public static void main(String[] args) {
	
	ApplicationContext context = new AnnotationConfigApplicationContext(DaoConfig.class);

	CourseDao courseDao = (CourseDao) context.getBean("courseDao");

	//Course course1 = new Course(10, "Art", 50, 1);
	//Course course2 = new Course(1, "Art", 20);
	//courseDao.insert(course1);
	System.out.println(courseDao.getById(1));
	//courseDao.delete(course1);
	//System.out.println(courseDao.getById(2));
	//Course course3 = courseDao.getById(1);
	//List<Course> courses = courseDao.getAll();
	/*System.out.println(course3);
	courses.stream().forEach(course -> {
	    System.out.println(course);
	});
	courseDao.delete(course1);
	courseDao.getAll().stream().forEach(course -> {
	    System.out.println(course);
	});*/
    }

}
