package com.gmail.snyp4eg.university.dao;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.jdbc.core.JdbcTemplate;

import com.gmail.snyp4eg.university.dao.interfaces.TeacherExtendedDao;
import com.gmail.snyp4eg.university.exception.DaoException;
import com.gmail.snyp4eg.university.model.Teacher;
import com.gmail.snyp4eg.university.reader.PropertyReader;
import com.gmail.snyp4eg.university.util.TeacherMapper;

public class TeacherDao implements TeacherExtendedDao {
  private static final String GET_BY_ID_KEY = "teacher.get-by-id";
  private static final String GET_ALL_KEY = "teacher.get-all";
  private static final String INSERT_KEY = "teacher.insert";
  private static final String UPDATE_KEY = "teacher.update";
  private static final String DELETE_KEY = "teacher.delete";
  private JdbcTemplate jdbcTemplate;
  private PropertyReader queryReader;
  
  @Autowired
  public TeacherDao(JdbcTemplate jdbcTemplate, PropertyReader queryReader) {
    this.jdbcTemplate = jdbcTemplate;
    this.queryReader = queryReader;
  }

  @Override
  public Teacher getById(Integer id) {
    Teacher teacher;
    try {
      teacher = jdbcTemplate.queryForObject(queryReader.read(GET_BY_ID_KEY), new TeacherMapper(), id);
    } catch (EmptyResultDataAccessException e) {
      throw new DaoException(e);
    }
    return teacher;
  }

  @Override
  public List<Teacher> getAll() {
    return jdbcTemplate.query(queryReader.read(GET_ALL_KEY), new TeacherMapper());
  }

  @Override
  public void insert(Teacher teacher) {
    jdbcTemplate.update(queryReader.read(INSERT_KEY), teacher.getId(), teacher.getEmployeeId());
  }

  @Override
  public void update(Teacher teacher) {
    jdbcTemplate.update(queryReader.read(UPDATE_KEY), teacher.getEmployeeId(), teacher.getId());
  }

  @Override
  public void delete(Teacher teacher) {
    jdbcTemplate.update(queryReader.read(DELETE_KEY), teacher.getId());
  }
}
