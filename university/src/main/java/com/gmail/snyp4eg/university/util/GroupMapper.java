package com.gmail.snyp4eg.university.util;

import java.sql.ResultSet;
import java.sql.SQLException;
import org.springframework.jdbc.core.RowMapper;
import com.gmail.snyp4eg.university.model.Group;

public class GroupMapper implements RowMapper<Group> {
    private static final String ID = "id";
    private static final String NAME = "name";

    @Override
    public Group mapRow(ResultSet rs, int rowNum) throws SQLException {
	Integer id = rs.getInt(ID);
	String name = rs.getString(NAME);
	return new Group(id, name);
    }

}
