package com.gmail.snyp4eg.university.model;

public class Student {
  private Integer id;
  private Integer groupId;
  private Integer courseId;
  private String firstName;
  private String lastName;

  public Student(Integer id, Integer groupId, Integer courseId, String firstName, String lastName) {
    this.id = id;
    this.groupId = groupId;
    this.courseId = courseId;
    this.firstName = firstName;
    this.lastName = lastName;
  }
  
  public Student(Integer id, Integer groupId, String firstName, String lastName) {
    this.id = id;
    this.groupId = groupId;
    this.firstName = firstName;
    this.lastName = lastName;
  }
  
  public Student(Integer id, String firstName, String lastName) {
    this.id = id;
    this.firstName = firstName;
    this.lastName = lastName;
  }
  
  public Student() {
    
  }

  public Integer getId() {
    return id;
  }

  public void setId(Integer id) {
    this.id = id;
  }

  public Integer getGroupId() {
    return groupId;
  }

  public void setGroupId(Integer groupId) {
    this.groupId = groupId;
  }

  public Integer getCourseId() {
    return courseId;
  }

  public void setCourseId(Integer courseId) {
    this.courseId = courseId;
  }

  public String getFirstName() {
    return firstName;
  }

  public void setFirstName(String firstName) {
    this.firstName = firstName;
  }

  public String getLastName() {
    return lastName;
  }

  public void setLastName(String lastName) {
    this.lastName = lastName;
  }

  @Override
  public boolean equals(Object o) {
    if (this == o)
      return true;
    if (o == null || getClass() != o.getClass())
      return false;
    Student student = (Student) o;
    return (this.id.equals(student.id) && this.groupId.equals(student.groupId) && this.courseId.equals(student.courseId)
        && this.firstName.equals(student.firstName) && this.lastName.equals(student.lastName));
  }

  @Override
  public int hashCode() {
    int result = 17;
    result = 31 * result + id.hashCode();
    result = 31 * result + groupId.hashCode();
    result = 31 * result + courseId.hashCode();
    result = 31 * result + firstName.hashCode();
    result = 31 * result + lastName.hashCode();
    return result;
  }

  @Override
  public String toString() {
    return "Student{" + "ID=" + id + '\'' + ", name='" + firstName + " " + lastName + '\'' + ", groupId='"
        + groupId + '}';
  }
}
