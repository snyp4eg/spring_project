package com.gmail.snyp4eg.university.model;

public class Position {
  private Integer id;
  private String name;

  public Position(Integer id, String name) {
    this.id = id;
    this.name = name;
  }
  
  public Position() {
    
  }

  public Integer getId() {
    return id;
  }

  public void setId(Integer id) {
    this.id = id;
  }

  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }

  @Override
  public boolean equals(Object o) {
    if (this == o)
      return true;
    if (o == null || getClass() != o.getClass())
      return false;
    Position position = (Position) o;
    return (this.id.equals(position.id) && this.name.equals(position.name));
  }

  @Override
  public int hashCode() {
    int result = 17;
    result = 31 * result + name.hashCode();
    result = 31 * result + id.hashCode();
    return result;
  }

  @Override
  public String toString() {
    return "Position {" + "ID=" + id + '\'' + ", name='" + name + '\'' + '}';
  }
}
