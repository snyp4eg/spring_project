package com.gmail.snyp4eg.university.dao;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.jdbc.core.JdbcTemplate;

import com.gmail.snyp4eg.university.dao.interfaces.GroupExtendedDao;
import com.gmail.snyp4eg.university.exception.DaoException;
import com.gmail.snyp4eg.university.model.Group;
import com.gmail.snyp4eg.university.reader.PropertyReader;
import com.gmail.snyp4eg.university.util.GroupMapper;

public class GroupDao implements GroupExtendedDao{
  private static final String GET_BY_ID_KEY = "group.get-by-id";
  private static final String GET_ALL_KEY = "group.get-all";
  private static final String INSERT_KEY = "group.insert";
  private static final String UPDATE_KEY = "group.update";
  private static final String DELETE_KEY = "group.delete";
    private JdbcTemplate jdbcTemplate;
    private PropertyReader queryReader;

    @Autowired
    public GroupDao(JdbcTemplate jdbcTemplate, PropertyReader queryReader) {
      this.jdbcTemplate = jdbcTemplate;
      this.queryReader = queryReader;
    }

  @Override
  public Group getById(Integer id) {
    Group group;
    try {
      group = jdbcTemplate.queryForObject(queryReader.read(GET_BY_ID_KEY), new GroupMapper(), id);
    }catch(EmptyResultDataAccessException e){
      throw new DaoException(e);
    }
    return group;
  }

  @Override
  public List<Group> getAll() {
    return jdbcTemplate.query(queryReader.read(GET_ALL_KEY), new GroupMapper());
  }

  @Override
  public void insert(Group group) {
    jdbcTemplate.update(queryReader.read(INSERT_KEY), group.getId(), group.getName());
  }

  @Override
  public void update(Group group) {
    jdbcTemplate.update(queryReader.read(UPDATE_KEY), group.getName(), group.getId());
  }

  @Override
  public void delete(Group group) {
    jdbcTemplate.update(queryReader.read(DELETE_KEY), group.getId());
  }
}
