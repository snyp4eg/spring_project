package com.gmail.snyp4eg.university.dao.interfaces;

import com.gmail.snyp4eg.university.model.Room;

public interface RoomExtendedDao extends GenericDao<Room>{
  
}
