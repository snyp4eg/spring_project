package com.gmail.snyp4eg.university.dao.interfaces;

import com.gmail.snyp4eg.university.model.RoomType;

public interface RoomTypeExtendedDao extends GenericDao<RoomType>{
  
}
