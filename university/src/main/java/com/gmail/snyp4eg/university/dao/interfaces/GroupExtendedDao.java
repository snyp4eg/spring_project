package com.gmail.snyp4eg.university.dao.interfaces;

import com.gmail.snyp4eg.university.model.Group;

public interface GroupExtendedDao extends GenericDao<Group>{
  
}
