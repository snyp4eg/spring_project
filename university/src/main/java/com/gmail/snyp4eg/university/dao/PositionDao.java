package com.gmail.snyp4eg.university.dao;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.jdbc.core.JdbcTemplate;

import com.gmail.snyp4eg.university.dao.interfaces.PositionExtendedDao;
import com.gmail.snyp4eg.university.exception.DaoException;
import com.gmail.snyp4eg.university.model.Position;
import com.gmail.snyp4eg.university.reader.PropertyReader;
import com.gmail.snyp4eg.university.util.PositionMapper;

public class PositionDao implements PositionExtendedDao{
  private static final String GET_BY_ID_KEY = "position.get-by-id";
  private static final String GET_ALL_KEY = "position.get-all";
  private static final String INSERT_KEY = "position.insert";
  private static final String UPDATE_KEY = "position.update";
  private static final String DELETE_KEY = "position.delete";
  private static final String POSITIONS_FOR_EMPLOYER_KEY = "position.positions-for-employer";
  private JdbcTemplate jdbcTemplate;
  private PropertyReader queryReader;
  
  @Autowired
  public PositionDao(JdbcTemplate jdbcTemplate, PropertyReader queryReader) {
    this.jdbcTemplate = jdbcTemplate;
    this.queryReader = queryReader;
  }
  
  @Override
  public Position getById(Integer id) {
    Position position;
    try {
      position = jdbcTemplate.queryForObject(queryReader.read(GET_BY_ID_KEY), new PositionMapper(), id);
    } catch (EmptyResultDataAccessException e) {
      throw new DaoException(e);
    }
    return position;
  }

  @Override
  public List<Position> getAll() {
    return jdbcTemplate.query(queryReader.read(GET_ALL_KEY), new PositionMapper());
  }

  @Override
  public void insert(Position position) {
    jdbcTemplate.update(queryReader.read(INSERT_KEY), position.getId(), position.getName());
  }

  @Override
  public void update(Position position) {
    jdbcTemplate.update(queryReader.read(UPDATE_KEY), position.getName(), position.getId());
  }

  @Override
  public void delete(Position position) {
    jdbcTemplate.update(queryReader.read(DELETE_KEY), position.getId());
  }

  @Override
  public List<Position> getPositionsListForEmployee(Integer employeeId) {
    return jdbcTemplate.query(queryReader.read(POSITIONS_FOR_EMPLOYER_KEY), new PositionMapper(), employeeId);
  }
}
