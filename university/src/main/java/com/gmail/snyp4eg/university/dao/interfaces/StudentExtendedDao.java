package com.gmail.snyp4eg.university.dao.interfaces;

import java.util.List;
import com.gmail.snyp4eg.university.model.Student;

public interface StudentExtendedDao extends GenericDao<Student>{
        
    List<Student> getCourseStudentList(Integer courseId);
}
