package com.gmail.snyp4eg.university.dao;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.jdbc.core.JdbcTemplate;

import com.gmail.snyp4eg.university.dao.interfaces.RoomTypeExtendedDao;
import com.gmail.snyp4eg.university.exception.DaoException;
import com.gmail.snyp4eg.university.model.RoomType;
import com.gmail.snyp4eg.university.reader.PropertyReader;
import com.gmail.snyp4eg.university.util.RoomTypeMapper;

public class RoomTypeDao implements RoomTypeExtendedDao{
  private static final String GET_BY_ID_KEY = "roomType.get-by-id";
  private static final String GET_ALL_KEY = "roomType.get-all";
  private static final String INSERT_KEY = "roomType.insert";
  private static final String UPDATE_KEY = "roomType.update";
  private static final String DELETE_KEY = "roomType.delete";
  private JdbcTemplate jdbcTemplate;
  private PropertyReader queryReader;
  
  @Autowired
  public RoomTypeDao(JdbcTemplate jdbcTemplate, PropertyReader queryReader) {
    this.jdbcTemplate = jdbcTemplate;
    this.queryReader = queryReader;
  }
  
  @Override
  public RoomType getById(Integer id) {
    RoomType position;
    try {
      position = jdbcTemplate.queryForObject(queryReader.read(GET_BY_ID_KEY), new RoomTypeMapper(), id);
    } catch (EmptyResultDataAccessException e) {
      throw new DaoException(e);
    }
    return position;
  }

  @Override
  public List<RoomType> getAll() {
    return jdbcTemplate.query(queryReader.read(GET_ALL_KEY), new RoomTypeMapper());
  }

  @Override
  public void insert(RoomType roomType) {
    jdbcTemplate.update(queryReader.read(INSERT_KEY), roomType.getId(), roomType.getName());
  }

  @Override
  public void update(RoomType roomType) {
    jdbcTemplate.update(queryReader.read(UPDATE_KEY), roomType.getName(), roomType.getId());
  }

  @Override
  public void delete(RoomType roomType) {
    jdbcTemplate.update(queryReader.read(DELETE_KEY), roomType.getId());
  }
}
