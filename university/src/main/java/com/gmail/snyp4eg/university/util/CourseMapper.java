package com.gmail.snyp4eg.university.util;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;

import com.gmail.snyp4eg.university.model.Course;

public class CourseMapper implements RowMapper<Course> {
    private static final String ID = "id";
    private static final String NAME = "name";
    private static final String CAPACITY = "capacity";
    private static final String TEACHER_ID = "teacher_id";

    @Override
    public Course mapRow(ResultSet rs, int rowNum) throws SQLException {
	Integer id = rs.getInt(ID);
	String name = rs.getString(NAME);
	Integer hoursCapacity = rs.getInt(CAPACITY);
	Integer teacherId = rs.getInt(TEACHER_ID);
	return new Course(id, name, hoursCapacity, teacherId);
    }
}
