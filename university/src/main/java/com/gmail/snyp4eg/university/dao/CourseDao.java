package com.gmail.snyp4eg.university.dao;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Component;

import com.gmail.snyp4eg.university.dao.interfaces.CourseExtendedDao;
import com.gmail.snyp4eg.university.model.Course;
import com.gmail.snyp4eg.university.reader.PropertyReader;
import com.gmail.snyp4eg.university.util.CourseMapper;
import com.gmail.snyp4eg.university.exception.DaoException;

@Component
public class CourseDao implements CourseExtendedDao {
    private static final String GET_BY_ID_KEY = "course.get-by-id";
    private static final String GET_ALL_KEY = "course.get-all";
    private static final String INSERT_KEY = "course.insert";
    private static final String UPDATE_KEY = "course.update";
    private static final String DELETE_KEY = "course.delete";
    private static final String TEACHER_COURSES_KEY = "course.teacher-courses";
    private JdbcTemplate jdbcTemplate;
    private PropertyReader queryReader;
    
    @Autowired
    public CourseDao(JdbcTemplate jdbcTemplate, PropertyReader queryReader) {
      this.jdbcTemplate = jdbcTemplate;
      this.queryReader = queryReader;
    }
    
    @Override
    public Course getById(Integer id) {
	Course course;
	try {
	    course = jdbcTemplate.queryForObject(queryReader.read(GET_BY_ID_KEY), new CourseMapper(), id);
	}catch(EmptyResultDataAccessException e){
	    throw new DaoException(e);
	}
	return course;
    }

    @Override
    public List<Course> getAll() {
	return jdbcTemplate.query(queryReader.read(GET_ALL_KEY), new CourseMapper());
    }

    @Override
    public void insert(Course course) {
	jdbcTemplate.update(queryReader.read(INSERT_KEY), course.getId(), course.getName(), course.getHoursCapacity(), course.getTeacherId());
    }

    @Override
    public void update(Course course) {
	jdbcTemplate.update(queryReader.read(UPDATE_KEY), course.getName(), course.getHoursCapacity(), course.getTeacherId(), course.getId());
    }

    @Override
    public void delete(Course course) {
	jdbcTemplate.update(queryReader.read(DELETE_KEY), course.getId());
    }

    @Override
    public List<Course> getTeacherCourses(Integer teacherId) {
	return jdbcTemplate.query(queryReader.read(TEACHER_COURSES_KEY), new CourseMapper());
    }
}
