package com.gmail.snyp4eg.university.util;

import java.sql.ResultSet;
import java.sql.SQLException;
import org.springframework.jdbc.core.RowMapper;
import com.gmail.snyp4eg.university.model.Student;

public class StudentMapper implements RowMapper<Student> {
    private static final String ID = "id";
    private static final String GROUP_ID = "group_id";
    private static final String COURSE_ID = "course_id";
    private static final String FIRST_NAME = "first_name";
    private static final String LAST_NAME = "last_name";

    @Override
    public Student mapRow(ResultSet rs, int rowNum) throws SQLException {
	Integer id = rs.getInt(ID);
	Integer groupId = rs.getInt(GROUP_ID);
	Integer courseId = rs.getInt(COURSE_ID);
	String firstName = rs.getString(FIRST_NAME);
	String lastName = rs.getString(LAST_NAME);
	return new Student(id, groupId, courseId, firstName, lastName);
    }

}
