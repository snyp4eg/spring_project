package com.gmail.snyp4eg.university.model;

public class Teacher {
  private Integer id;
  private Integer employeeId;

  public Teacher(Integer id, Integer employeeId) {
    this.id = id;
    this.employeeId = employeeId;
  }

  public Integer getId() {
    return id;
  }

  public void setId(Integer id) {
    this.id = id;
  }

  public Integer getEmployeeId() {
    return employeeId;
  }

  public void setEmployeeId(Integer employeeId) {
    this.employeeId = employeeId;
  }
  
  @Override
  public boolean equals(Object o) {
    if (this == o)
      return true;
    if (o == null || getClass() != o.getClass())
      return false;
    Teacher teacher = (Teacher) o;
    return (this.id.equals(teacher.id) && this.employeeId.equals(teacher.employeeId));
  }

  @Override
  public int hashCode() {
    int result = 17;
    result = 31 * result + id.hashCode();
    result = 31 * result + employeeId.hashCode();
    return result;
  }

  @Override
  public String toString() {
    return "Teacher {" + "ID=" + id + '\'' + ", Employee id:" + employeeId  + '}';
  }
}
