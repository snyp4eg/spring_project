package com.gmail.snyp4eg.university.dao.interfaces;

import java.util.List;
import com.gmail.snyp4eg.university.model.Course;

public interface CourseExtendedDao extends GenericDao<Course>{
        
    List<Course> getTeacherCourses(Integer teacherId);
}
