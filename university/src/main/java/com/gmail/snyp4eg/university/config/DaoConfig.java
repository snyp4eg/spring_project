package com.gmail.snyp4eg.university.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.datasource.DriverManagerDataSource;

import com.gmail.snyp4eg.university.dao.CourseDao;
import com.gmail.snyp4eg.university.dao.EmployeeDao;
import com.gmail.snyp4eg.university.dao.GroupDao;
import com.gmail.snyp4eg.university.dao.PositionDao;
import com.gmail.snyp4eg.university.dao.RoomDao;
import com.gmail.snyp4eg.university.dao.RoomTypeDao;
import com.gmail.snyp4eg.university.dao.StudentDao;
import com.gmail.snyp4eg.university.dao.TeacherDao;
import com.gmail.snyp4eg.university.reader.PropertyReader;

@Configuration
public class DaoConfig {
    protected static final String DRIVER_CLASS_NAME = "org.postgresql.Driver";
    protected static final String URL = "jdbc:postgresql://localhost:5432/University";
    protected static final String USER_NAME = "taskuser";
    protected static final String PASSWORD = "313233";
    protected static final String QUERY_PATH = "./src/main/resources/sql/query.properties";

    @Bean
    public DriverManagerDataSource dataSourse() {
	DriverManagerDataSource getDataSourse = new DriverManagerDataSource();
	getDataSourse.setDriverClassName(DRIVER_CLASS_NAME);
	getDataSourse.setUrl(URL);
	getDataSourse.setUsername(USER_NAME);
	getDataSourse.setPassword(PASSWORD);
	return getDataSourse;
    }

    @Bean
    public JdbcTemplate jdbcTemplate() {
	return new JdbcTemplate(dataSourse());
    }

    @Bean
    public PropertyReader queryReader() {
	return new PropertyReader(QUERY_PATH);
    }

    @Bean
    public CourseDao courseDao() {
	return new CourseDao(jdbcTemplate(), queryReader());
    }

    @Bean
    public EmployeeDao employeeDao() {
	return new EmployeeDao(jdbcTemplate(), queryReader());
    }

    @Bean
    public GroupDao groupDao() {
	return new GroupDao(jdbcTemplate(), queryReader());
    }

    @Bean
    public PositionDao positionDao() {
	return new PositionDao(jdbcTemplate(), queryReader());
    }

    @Bean
    public RoomDao roomDao() {
	return new RoomDao(jdbcTemplate(), queryReader());
    }

    @Bean
    public RoomTypeDao roomTypeDao() {
	return new RoomTypeDao(jdbcTemplate(), queryReader());
    }

    @Bean
    public StudentDao studentDao() {
	return new StudentDao(jdbcTemplate(), queryReader());
    }

    @Bean
    public TeacherDao teacherDao() {
	return new TeacherDao(jdbcTemplate(), queryReader());
    }
}
