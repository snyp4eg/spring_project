package com.gmail.snyp4eg.university.util;

import java.sql.ResultSet;
import java.sql.SQLException;
import org.springframework.jdbc.core.RowMapper;
import com.gmail.snyp4eg.university.model.Room;

public class RoomMapper implements RowMapper<Room> {
    private static final String ID = "id";
    private static final String CAPACITY = "capacity";
    private static final String ROOM_TYPE_ID = "room_type_id";

    @Override
    public Room mapRow(ResultSet rs, int rowNum) throws SQLException {
	Integer id = rs.getInt(ID);
	Integer capacity = rs.getInt(CAPACITY);
	Integer roomTypeId = rs.getInt(ROOM_TYPE_ID);
	return new Room(id, capacity, roomTypeId);
    }
}
