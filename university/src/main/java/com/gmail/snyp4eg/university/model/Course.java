package com.gmail.snyp4eg.university.model;

public class Course {
  private Integer id;
  private String name;
  private Integer hoursCapacity;
  private Integer teacherId;

  public Course() {
  }

  public Course(Integer id, String name, Integer hoursCapacity, Integer teacherId) {
    this.id = id;
    this.name = name;
    this.hoursCapacity = hoursCapacity;
    this.teacherId = teacherId;
  }

  public int getId() {
    return id;
  }

  public void setId(Integer id) {
    this.id = id;
  }

  public String getName() {
    return name;
  }

  public void setName(String courseName) {
    this.name = courseName;
  }

  public Integer getHoursCapacity() {
    return hoursCapacity;
  }

  public void setHoursCapacity(Integer hoursCapacity) {
    this.hoursCapacity = hoursCapacity;
  }

  @Override
  public boolean equals(Object o) {
    if (this == o)
      return true;
    if (o == null || getClass() != o.getClass())
      return false;
    Course course = (Course) o;
    return (this.id.equals(course.id) && this.name.equals(course.name)
        && this.hoursCapacity.equals(course.hoursCapacity));
  }

  @Override
  public int hashCode() {
    int result = 17;
    result = 31 * result + id.hashCode();
    result = 31 * result + name.hashCode();
    result = 31 * result + hoursCapacity.hashCode();
    return result;
  }

  @Override
  public String toString() {
    return ("Course{ ID=" + id + ", name= " + name + ", hours capacity = " + hoursCapacity + "}");
  }

public Integer getTeacherId() {
    return teacherId;
}

public void setTeacherId(Integer teacherId) {
    this.teacherId = teacherId;
}

}
