package com.gmail.snyp4eg.university.dao;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.jdbc.core.JdbcTemplate;

import com.gmail.snyp4eg.university.dao.interfaces.EmployeeExtendedDao;
import com.gmail.snyp4eg.university.exception.DaoException;
import com.gmail.snyp4eg.university.model.Employee;
import com.gmail.snyp4eg.university.reader.PropertyReader;
import com.gmail.snyp4eg.university.util.EmployeeMapper;

public class EmployeeDao implements EmployeeExtendedDao {
  private static final String GET_BY_ID_KEY = "employee.get-by-id";
  private static final String GET_ALL_KEY = "employee.get-all";
  private static final String INSERT_KEY = "employee.insert";
  private static final String UPDATE_KEY = "employee.update";
  private static final String DELETE_KEY = "employee.delete";
  private static final String EMPLOYERS_ON_POSITION_KEY = "employee.employers-on-position";
  private JdbcTemplate jdbcTemplate;
  private PropertyReader queryReader;

  @Autowired
  public EmployeeDao(JdbcTemplate jdbcTemplate, PropertyReader queryReader) {
    this.jdbcTemplate = jdbcTemplate;
    this.queryReader = queryReader;
  }
  @Override
  public Employee getById(Integer id) {
    Employee employee;
    try {
      employee = jdbcTemplate.queryForObject(queryReader.read(GET_BY_ID_KEY), new EmployeeMapper(), id);
    } catch (EmptyResultDataAccessException e) {
      throw new DaoException(e);
    }
    return employee;
  }

  @Override
  public List<Employee> getAll() {
    return jdbcTemplate.query(queryReader.read(GET_ALL_KEY), new EmployeeMapper());
  }

  @Override
  public void insert(Employee employee) {
    jdbcTemplate.update(queryReader.read(INSERT_KEY), employee.getId(), employee.getFirstName(),
        employee.getLastName());
  }

  @Override
  public void update(Employee employee) {
    jdbcTemplate.update(queryReader.read(UPDATE_KEY), employee.getFirstName(), employee.getLastName(),
        employee.getId());
  }

  @Override
  public void delete(Employee employee) {
    jdbcTemplate.update(queryReader.read(DELETE_KEY), employee.getId());
  }

  @Override
  public List<Employee> getEmployersOnPositionList(Integer positionId) {
    return jdbcTemplate.query(queryReader.read(EMPLOYERS_ON_POSITION_KEY), new EmployeeMapper(), positionId);
  }
}
