package com.gmail.snyp4eg.university.model;

public class Group {
  private Integer id;
  private String name;

  public Group(Integer id, String name) {
    this.id = id;
    this.name = name;
  }
  
  public Group() {
    
  }

  public int getId() {
    return id;
  }

  public void setiId(Integer id) {
    this.id = id;
  }

  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }

  @Override
  public boolean equals(Object o) {
    if (this == o)
      return true;
    if (o == null || getClass() != o.getClass())
      return false;
    Group group = (Group) o;
    return (this.id.equals(group.id) && this.name.equals(group.name));
  }

  @Override
  public int hashCode() {
    int result = 17;
    result = 31 * result + id.hashCode();
    result = 31 * result + name.hashCode();
    return result;
  }

  @Override
  public String toString() {
    return "Group{" + "ID= " + id + '\'' + ", name= '" + name + "}";
  }
}
