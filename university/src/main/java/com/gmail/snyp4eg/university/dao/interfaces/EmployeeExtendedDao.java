package com.gmail.snyp4eg.university.dao.interfaces;

import java.util.List;
import com.gmail.snyp4eg.university.model.Employee;

public interface EmployeeExtendedDao extends GenericDao<Employee> {
        
    List<Employee> getEmployersOnPositionList(Integer positionId);
}
