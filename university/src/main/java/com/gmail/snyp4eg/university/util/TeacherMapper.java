package com.gmail.snyp4eg.university.util;

import java.sql.ResultSet;
import java.sql.SQLException;
import org.springframework.jdbc.core.RowMapper;
import com.gmail.snyp4eg.university.model.Teacher;

public class TeacherMapper implements RowMapper<Teacher> {
    private static final String ID = "id";
    private static final String EMPLOYEE_ID = "employee_id";

    @Override
    public Teacher mapRow(ResultSet rs, int rowNum) throws SQLException {
	Integer id = rs.getInt(ID);
	Integer employeeId = rs.getInt(EMPLOYEE_ID);
	return new Teacher(id, employeeId);
    }
}
