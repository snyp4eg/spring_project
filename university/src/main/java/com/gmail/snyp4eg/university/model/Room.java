package com.gmail.snyp4eg.university.model;

public class Room {
  private Integer id;
  private Integer capacity;
  private Integer roomTypeId;

  public Room(Integer id, Integer capacity, Integer roomTypeId) {
    this.id = id;
    this.capacity = capacity;
    this.roomTypeId = roomTypeId;
  }
  
  public Room() {
    
  }

  public Integer getId() {
    return id;
  }

  public void setId(Integer id) {
    this.id = id;
  }

  public Integer getCapacity() {
    return capacity;
  }

  public void setCapacity(Integer capacity) {
    this.capacity = capacity;
  }

  public Integer getRoomTypeId() {
    return roomTypeId;
  }

  public void setRoomType(Integer roomTypeId) {
    this.roomTypeId = roomTypeId;
  }

  @Override
  public boolean equals(Object o) {
    if (this == o)
      return true;
    if (o == null || getClass() != o.getClass())
      return false;
    Room room = (Room) o;
    return (this.id.equals(room.id) && this.capacity.equals(room.capacity) && this.roomTypeId.equals(room.roomTypeId));
  }

  @Override
  public int hashCode() {
    int result = 17;
    result = 31 * result + id.hashCode();
    result = 31 * result + capacity.hashCode();
    result = 31 * result + roomTypeId.hashCode();
    return result;
  }

  @Override
  public String toString() {
    return "Employee {" + "ID=" + id + '\'' + ", capacity='" + capacity + '\'' + ", room type= '" + roomTypeId + '}';
  }
}
