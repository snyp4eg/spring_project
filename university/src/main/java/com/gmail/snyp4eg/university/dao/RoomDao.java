package com.gmail.snyp4eg.university.dao;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.jdbc.core.JdbcTemplate;

import com.gmail.snyp4eg.university.dao.interfaces.RoomExtendedDao;
import com.gmail.snyp4eg.university.exception.DaoException;
import com.gmail.snyp4eg.university.model.Room;
import com.gmail.snyp4eg.university.reader.PropertyReader;
import com.gmail.snyp4eg.university.util.RoomMapper;

public class RoomDao implements RoomExtendedDao {
    private static final String GET_BY_ID_KEY = "room.get-by-id";
    private static final String GET_ALL_KEY = "room.get-all";
    private static final String INSERT_KEY = "room.insert";
    private static final String UPDATE_KEY = "room.update";
    private static final String DELETE_KEY = "room.delete";
    private JdbcTemplate jdbcTemplate;
    private PropertyReader queryReader;
   
    @Autowired
    public RoomDao(JdbcTemplate jdbcTemplate, PropertyReader queryReader) {
      this.jdbcTemplate = jdbcTemplate;
      this.queryReader = queryReader;
    }
    
    @Override
    public Room getById(Integer id) {
	Room room;
	try {
	    room = jdbcTemplate.queryForObject(queryReader.read(GET_BY_ID_KEY), new RoomMapper(), id);
	} catch (EmptyResultDataAccessException e) {
	  throw new DaoException(e);
	}
	return room;
    }

    @Override
    public List<Room> getAll() {
	return jdbcTemplate.query(queryReader.read(GET_ALL_KEY), new RoomMapper());
    }

    @Override
    public void insert(Room room) {
	jdbcTemplate.update(queryReader.read(INSERT_KEY), room.getId(), room.getCapacity(), room.getRoomTypeId());

    }

    @Override
    public void update(Room room) {
	jdbcTemplate.update(queryReader.read(UPDATE_KEY), room.getCapacity(), room.getRoomTypeId(), room.getId());
    }

    @Override
    public void delete(Room room) {
	jdbcTemplate.update(queryReader.read(DELETE_KEY), room.getId());
    }
}
