package com.gmail.snyp4eg.university.model;

public class RoomType {
  private Integer id;
  private String name;

  public RoomType(Integer id, String name) {
    this.id = id;
    this.name = name;
  }
  
  public RoomType() {
    
  }

  public Integer getId() {
    return id;
  }

  public void setId(Integer id) {
    this.id = id;
  }

  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }

  @Override
  public boolean equals(Object o) {
    if (this == o)
      return true;
    if (o == null || getClass() != o.getClass())
      return false;
    RoomType roomType = (RoomType) o;
    return (this.id.equals(roomType.id) && this.name.equals(roomType.name));
  }

  @Override
  public int hashCode() {
    int result = 17;
    result = 31 * result + id.hashCode();
    result = 31 * result + name.hashCode();
    return result;
  }

  @Override
  public String toString() {
    return "Employee {" + "ID=" + id + '\'' + ", name='" + name + '\'' + '}';
  }
}
